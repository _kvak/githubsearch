//
//  RepoEntity.swift
//  GithubRepoSearch
//
//  Created by Andre Kvashuk on 10/10/18.
//  Copyright © 2018 Andre Kvashuk. All rights reserved.
//

import Foundation
import SwiftyJSON
import CoreData

@objc class RepoEntity: NSManagedObject {
//    init(urlString: String, name: String, id: EntityId, stars: Int) {
//        self.urlString = urlString
//        self.name = name
//        self.id = id
//        self.stars = stars
//    }
    
    static func requestWith(repoId: Int64) -> NSFetchRequest<RepoEntity> {
        let request: NSFetchRequest<RepoEntity> = RepoEntity.fetchRequest()
        request.predicate = NSPredicate(format: "entityId == %ld", repoId)
        
        return request
    }
    
    static func from(json: JSON, in context: NSManagedObjectContext) -> RepoEntity? {
        guard let id = json["id"].int64 else {
            return nil
        }
        guard let urlString = json["html_url"].string else {
            return nil
        }
        guard let name = json["name"].string else {
            return nil
        }
        guard let stars = json["stargazers_count"].int32 else {
            return nil
        }
        
        do {
            let countForId = try context.count(for: RepoEntity.requestWith(repoId: id))
            let totalTotal = try context.count(for: RepoEntity.fetchRequest())
            
            if countForId == 0 {
                let description = NSEntityDescription.entity(forEntityName: "RepoEntity", in: context)!
                let repo = RepoEntity(entity: description, insertInto: context)
                repo.initWith(id: id, name: name, stars: stars, url: urlString, sortIndex: Int32(totalTotal))
                
                return repo
            } else {
                let result = try? RepoEntity.entityWith(id: id, context: context)
                
                return result
            }
            
        } catch {
            return nil
        }
    }
    
    private func initWith(id: Int64, name: String, stars: Int32, url: String, sortIndex: Int32) {
        self.entityId = id
        self.name = name
        self.stars = stars
        self.urlString = url
        self.sortIndex = sortIndex
    }
    
    static func entityWith(id: Int64, context: NSManagedObjectContext) throws -> RepoEntity {
        let request = RepoEntity.requestWith(repoId: id)
        
        do {
            let result = try context.fetch(request)
            
            guard result.count == 1 else {
                throw LocalError.coreDatafetchFailure
            }
            
            return result.first!
        } catch let error {
            throw error
        }
        
    }
}

extension RepoEntity: HomeViewEntity {
    var isViewed: Bool {
        return false
    }
    
    var repoName: String {
        return self.name!
    }
    
    var repoUrl: String {
        return self.urlString!
    }
    
    var repoId: Int64 {
        return self.entityId
    }
    
    var repoStars: Int32 {
        return self.stars
    }
}

extension RepoEntity: RepoDetailEntity {
    
    var url: URL? {
        return URL(string: self.urlString!)
    }
    
}
