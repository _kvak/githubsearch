//
//  UIView+Ext.swift
//  GithubRepoSearch
//
//  Created by Andre Kvashuk on 10/14/18.
//  Copyright © 2018 Andre Kvashuk. All rights reserved.
//

import UIKit

extension UIView {
    
    class func defaultNib() -> UINib {
        return UINib(nibName: "\(self)", bundle: nil)
    }
    
    class func viewFromNib() -> Self {
        return viewWithType()
    }
}

private extension UIView {
    
    class func viewWithType<T>() -> T {
        return self.defaultNib().instantiate(withOwner: nil, options: nil).first as! T
    }
    
}
