//
//  RepoDetailConfigurator.swift
//  GithubRepoSearch
//
//  Created by Andre Kvashuk on 10/15/18.
//  Copyright © 2018 Andre Kvashuk. All rights reserved.
//

import Foundation

protocol RepoDetailConfiguratorProtocol {
    func configure(with viewController: RepoDetailViewController)
}

class RepoDetailConfigurator: RepoDetailConfiguratorProtocol {
    
    func configure(with viewController: RepoDetailViewController) {
        let presenter = RepoDetailPresenter(view: viewController)
        let interactor = RepoDetailInteractor(presenter: presenter)
        let router = RepoDetailRouter(viewController: viewController)
        
        presenter.interactor = interactor
        presenter.router = router
        viewController.presenter = presenter
    }
}
