//
//  RepoDetailInteractor.swift
//  GithubRepoSearch
//
//  Created by Andre Kvashuk on 10/15/18.
//  Copyright © 2018 Andre Kvashuk. All rights reserved.
//

import Foundation

protocol RepoDetailInteractorProtocol {
    
}

class RepoDetailInteractor: RepoDetailInteractorProtocol {
    private weak var presenter: RepoDetailPresenterProtocol!
    
    required init(presenter: RepoDetailPresenter) {
        self.presenter = presenter
    }
}
