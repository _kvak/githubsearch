//
//  RepoDetailPresenter.swift
//  GithubRepoSearch
//
//  Created by Andre Kvashuk on 10/15/18.
//  Copyright © 2018 Andre Kvashuk. All rights reserved.
//

import Foundation

protocol RepoDetailPresenterProtocol: class {
    var router: RepoDetailRouterProtocol! { set get }
    
    func configureView()
    func set(url: URL)
    
    func onCloseButton()
}

class RepoDetailPresenter: RepoDetailPresenterProtocol {
    var router: RepoDetailRouterProtocol!
    var interactor: RepoDetailInteractorProtocol!
    weak var view: RepoDetailViewProtocol!
    
    required init(view: RepoDetailViewProtocol) {
        self.view = view
    }
    
    func configureView() {
        
    }
    
    func set(url: URL) {
        let request = URLRequest(url: url)
        self.view.load(request: request)
    }
    
    func onCloseButton() {
        self.router.close()
    }
}
