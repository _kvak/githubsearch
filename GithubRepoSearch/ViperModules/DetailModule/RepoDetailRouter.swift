//
//  RepoDetailRouter.swift
//  GithubRepoSearch
//
//  Created by Andre Kvashuk on 10/15/18.
//  Copyright © 2018 Andre Kvashuk. All rights reserved.
//

import Foundation

protocol RepoDetailRouterProtocol: class {
    func close()
}

class RepoDetailRouter: RepoDetailRouterProtocol {
    private var viewController: RepoDetailViewController
    
    required init(viewController: RepoDetailViewController) {
        self.viewController = viewController
    }
    
    func close() {
        self.viewController.dismiss(animated: true, completion: nil)
    }
}
