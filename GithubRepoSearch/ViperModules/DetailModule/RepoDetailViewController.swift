//
//  RepoDetailViewController.swift
//  GithubRepoSearch
//
//  Created by Andre Kvashuk on 10/15/18.
//  Copyright © 2018 Andre Kvashuk. All rights reserved.
//

import UIKit
import WebKit

protocol RepoDetailEntity {
    var url: URL? { get }
}

protocol RepoDetailViewProtocol: class {
    func load(request: URLRequest)
}

class RepoDetailViewController: UIViewController {
    
    var presenter: RepoDetailPresenterProtocol!
    private let configurator: RepoDetailConfiguratorProtocol = RepoDetailConfigurator()
    
    @IBOutlet weak var fadeView: UIView!
    @IBOutlet weak var webView: WKWebView!
    private var initialURL: URL?
    private var failureURL = URL(string: "http://github.com")
    
    fileprivate var normalFadeColor: UIColor?
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        
        self.modalTransitionStyle = .crossDissolve
        self.modalPresentationStyle = .overCurrentContext
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        self.modalTransitionStyle = .crossDissolve
        self.modalPresentationStyle = .overCurrentContext
    }
    
    func set(model: RepoDetailEntity) {
        self.initialURL = model.url ?? failureURL
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configurator.configure(with: self)
        presenter.set(url: initialURL!)
        presenter.configureView()
        normalFadeColor = fadeView.backgroundColor
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        let transform = CGAffineTransform(scaleX: 0.001, y: 0.001)
        webView.transform = transform
        fadeView.backgroundColor = UIColor.clear
        
        UIView.animate(withDuration: 0.3) {
            self.webView.transform = .identity
            self.fadeView.backgroundColor = self.normalFadeColor
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        let transform = CGAffineTransform(scaleX: 0.001, y: 0.001)
       
        UIView.animate(withDuration: 0.3) {
            self.webView.transform = transform
            self.fadeView.backgroundColor = UIColor.clear
        }
        
        super.viewWillDisappear(animated)
    }
    
    @IBAction func handleSwipe(_ sender: Any) {
        self.presenter.onCloseButton()
    }
    
    @IBAction func handleCloseButton(_ sender: Any) {
        self.presenter.onCloseButton()
    }
    
}

extension RepoDetailViewController: RepoDetailViewProtocol {
    
    func load(request: URLRequest) {
        self.webView.load(request)
    }
    
}
