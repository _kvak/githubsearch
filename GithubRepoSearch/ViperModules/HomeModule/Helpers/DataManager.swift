//
//  APIManager.swift
//  GithubRepoSearch
//
//  Created by Andre Kvashuk on 10/16/18.
//  Copyright © 2018 Andre Kvashuk. All rights reserved.
//

import Foundation
import CoreData
import SwiftyJSON

protocol RepoSearchClient {
    func isLoggedIn() -> Bool
    func searchRepos(keyword: String, page: Int, pageSize: Int, success: @escaping ([RepoEntity]) -> (), failure: @escaping (Error) -> ())
    func authorizeWith(code: String, completion: @escaping (Error?) -> ())
    func removeToken()
}

enum LocalError {
    static let unauthorized = NSError(domain: "local", code: -1, userInfo: [NSLocalizedDescriptionKey: "Not authorized"])
    static let jsonFailure = NSError(domain: "local", code: -1, userInfo: [NSLocalizedDescriptionKey: "Unable to parse JSON"])
    static let coreDatafetchFailure = NSError(domain: "local", code: -1, userInfo: [NSLocalizedDescriptionKey: "No object for entityID"])
}

class DataManager: RepoSearchClient {
    
    private let clientId = "Iv1.a5e15d174b9064c5"
    private let redirect = "https://githubsearchapp-62742.firebaseapp.com/__/auth/handler"
    private let secret = "d7119599cefdff91708809c48e39eac2f39ebbd3"
    private let accessTokenKey = "accessTokenKey"
    
    private class ResponseContainer {
        let responseQueue: DispatchQueue = DispatchQueue(label: "DataManagerResponseQueue")
        
        private(set) var responseCount = 0
        private(set) var results = [RepoEntity]()
        
        var isReady: Bool {
            return responseCount == 2
        }
        
        func add(result: [RepoEntity]) {
            responseQueue.sync {
                self.results.append(contentsOf: result)
                
                self.responseCount += 1
            }
        }
    }
    
    let context: NSManagedObjectContext = {
        let moc = NSManagedObjectContext(concurrencyType: NSManagedObjectContextConcurrencyType.privateQueueConcurrencyType)
        moc.parent = AppDelegate.context
        moc.mergePolicy = NSMergeByPropertyObjectTrumpMergePolicy
        
        return moc
    }()
    
    func isLoggedIn() -> Bool {
        return getAccessToken() != nil
    }
    
    func getAccessToken() -> String? {
        return UserDefaults.standard.value(forKey: accessTokenKey) as? String
    }
    
    func removeToken() {
        set(accessToken: nil)
    }
    
    func set(accessToken: String?) {
        UserDefaults.standard.set(accessToken, forKey: self.accessTokenKey)
    }
    
    private func privateSearchRepos(keyword: String,
                            page: Int,
                            pageSize: Int,
                            responseContainer: ResponseContainer,
                            success: @escaping ([RepoEntity]) -> (),
                            failure: @escaping (Error) -> ()) {
        
        guard let token = getAccessToken() else {
            failure(LocalError.unauthorized)
            
            return
        }
        let encodedQuery = keyword.trimmingCharacters(in: .whitespacesAndNewlines).addingPercentEncoding(withAllowedCharacters: .urlPathAllowed)!
        let urlString = "https://api.github.com/search/repositories?q=\(encodedQuery)&page=\(page)&per_page=15&sort=stars"
        let url = URL(string: urlString)!
        var urlRequest = URLRequest(url: url, cachePolicy: .returnCacheDataElseLoad, timeoutInterval: 30)
        urlRequest.setValue(token, forHTTPHeaderField: "access_token")
        
        let task = URLSession.shared.dataTask(with: urlRequest) { (data, response, error) in
            guard let data = data, error == nil else {
                DispatchQueue.main.async {
                    failure(error!)
                }
                return
            }
            
            guard let json = try? JSON(data: data) else {
                DispatchQueue.main.async {
                    failure(LocalError.jsonFailure)
                }
                
                return
            }
            
            self.context.performAndWait { [unowned self] in
                let result = json["items"].arrayValue.compactMap { RepoEntity.from(json: $0, in: self.context) }
                try! self.context.save()
                responseContainer.add(result: result)
                
                if responseContainer.isReady {
                    DispatchQueue.main.async {
                        success(responseContainer.results)
                    }
                }
            }
        }
        
        task.resume()
    }
    
    func searchRepos(keyword: String, page: Int, pageSize: Int, success: @escaping ([RepoEntity]) -> (), failure: @escaping (Error) -> ()) {
        guard self.isLoggedIn() else {
            failure(LocalError.unauthorized)
            
            return
        }
        
        let responseContainer = ResponseContainer()
        let queues = [DispatchQueue.global(qos: .userInitiated),
                      DispatchQueue.global(qos: .default)]
        
        let count = queues.count
        
        queues.enumerated().forEach { enumerat in
            enumerat.element.async {
                let page = page + enumerat.offset
                let size = pageSize / count
                
                self.privateSearchRepos(keyword: keyword, page: page, pageSize: size, responseContainer: responseContainer, success: success, failure: failure)
            }
        }
    }
    
    func authorizeWith(code: String, completion: @escaping (Error?) -> ()) {
        let urlString = "https://github.com/login/oauth/access_token?code=\(code)&client_id=\(clientId)&client_secret=\(secret)"
        let url = URL(string: urlString)!
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = "POST"
        
        let task = URLSession.shared.dataTask(with: urlRequest) { data, response, error in
            guard let data = data, error == nil else {
                DispatchQueue.main.async {
                    completion(error)
                }
                
                return
            }
            
            let responseString = String(data: data, encoding: .utf8)!
            let paramsString = responseString.components(separatedBy: "&")
            let keyValuesString = paramsString.compactMap { $0.components(separatedBy: "=") }
            let dictArray = keyValuesString.compactMap { [ $0.first! : $0.last! ] }
            let accessTokenDict = dictArray.first { $0.keys.contains("access_token") }
            let accessToken = accessTokenDict?["access_token"]
            
            self.set(accessToken: accessToken)
            DispatchQueue.main.async {
                completion(nil)
            }
        }
        
        task.resume()
    }
}
