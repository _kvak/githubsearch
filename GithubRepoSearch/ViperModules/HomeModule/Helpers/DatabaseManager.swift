//
//  DatabaseManager.swift
//  GithubRepoSearch
//
//  Created by Andre Kvashuk on 10/16/18.
//  Copyright © 2018 Andre Kvashuk. All rights reserved.
//

import Foundation
import CoreData

protocol DatabaseRepoClient {
    func searchDBRepos(query: String) throws -> [RepoEntity]
    func deleteEntity(repo: RepoEntity)
    func fetchAll() throws -> [RepoEntity]
}

class DatabaseManager: DatabaseRepoClient {
    func fetchAll() throws -> [RepoEntity] {
        let fetchRequest: NSFetchRequest<RepoEntity> = RepoEntity.fetchRequest()
        let sortDescriptor = NSSortDescriptor(key: "sortIndex", ascending: false)
        fetchRequest.sortDescriptors = [sortDescriptor]
        
        do {
            let result = try AppDelegate.context.fetch(fetchRequest)
            
            return result
        } catch let error {
            throw error
        }
    }
    
    func searchDBRepos(query: String) throws -> [RepoEntity] {
        let fetchRequest: NSFetchRequest<RepoEntity> = RepoEntity.fetchRequest()
        
        let sortDescriptor = NSSortDescriptor(key: "sortIndex", ascending: true)
        
        fetchRequest.predicate = NSPredicate(format: "name contains[c] %@", query)
        fetchRequest.sortDescriptors = [sortDescriptor]
        
        do {
            let result = try AppDelegate.context.fetch(fetchRequest)
            
            return result
        } catch let error {
            throw error
        }
    }
    
    func deleteEntity(repo: RepoEntity) {
        let context = AppDelegate.context
        context.delete(repo)
        
        try? context.save()
    }
}
