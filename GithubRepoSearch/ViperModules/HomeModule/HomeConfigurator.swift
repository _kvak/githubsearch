//
//  HomeConfigurator.swift
//  GithubRepoSearch
//
//  Created by Andre Kvashuk on 10/10/18.
//  Copyright © 2018 Andre Kvashuk. All rights reserved.
//

import Foundation

class HomeConfigurator: HomeConfiguratorProtocol {

    func configure(with viewController: HomeViewController) {
        let presenter = HomePresenter(view: viewController)
        let interactor = HomeInteractor(presenter: presenter)
        let router = HomeRouter(viewController: viewController)
        
        presenter.interactor = interactor
        presenter.router = router
        viewController.presenter = presenter
    }
}
