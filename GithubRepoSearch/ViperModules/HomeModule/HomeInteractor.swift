//
//  HomeInteractor.swift
//  GithubRepoSearch
//
//  Created by Andre Kvashuk on 10/10/18.
//  Copyright © 2018 Andre Kvashuk. All rights reserved.
//

import Foundation
import Firebase
import SwiftyJSON


class HomeInteractor: HomeInteractorProtocol {
    
    let dataManager: RepoSearchClient = DataManager()
    let dbManager: DatabaseRepoClient = DatabaseManager()
    
    private weak var presenter: HomePresenterProtocol!
    
    required init(presenter: HomePresenterProtocol) {
        self.presenter = presenter
    }
    
    func isLoggedIn() -> Bool {
        return self.dataManager.isLoggedIn()
    }
    
    func searchRepos(keyword: String, page: Int, pageSize: Int) {
        if self.dataManager.isLoggedIn() {
            searchWebRepos(keyword: keyword, page: page, pageSize: pageSize)
        } else {
            if page > 0 { return }
            searchDatabaseRepos(keyword: keyword)
        }
    }
    
    func searchDatabaseRepos(keyword: String) {
        do {
            let result = try dbManager.searchDBRepos(query: keyword)
            
            let wrappedRepos = result.compactMap { RepoEntityWrapper(repo: $0) }
            self.presenter.set(results: wrappedRepos, query: keyword, page: 0)
        } catch let error {
            self.presenter.showAlertView(with: error.localizedDescription)
        }
    }
    
    func searchWebRepos(keyword: String, page: Int, pageSize: Int) {
        dataManager.searchRepos(keyword: keyword, page: page, pageSize: pageSize, success: { repos in
            
            DispatchQueue.main.async {
                self.presenter.set(results: repos, query: keyword, page: page)
            }
            
        }) { (error) in
            DispatchQueue.main.async {
                self.presenter.showAlertView(with: error.localizedDescription)
            }
        }
    }
    
    func authorizeWith(code: String) {
        self.dataManager.authorizeWith(code: code) { (error) in
            if let error = error {
                self.presenter.showAlertView(with: error.localizedDescription)
            } else {
                self.presenter.set(isLoggedIn: true)
                self.presenter.showAlertView(with: "Success")
            }
        }
    }
    
    func logout() {
        self.dataManager.removeToken()
        self.presenter.set(isLoggedIn: false)
        self.presenter.showAlertView(with: "Logout successful")
    }
    
    func deleteFromDB(wrapper: RepoEntityWrapper) {
        self.dbManager.deleteEntity(repo: wrapper.repo)
    }
    
    func getRecentRepos() {
        do {
            let results = try self.dbManager.fetchAll()
            let wrappedArr = results.compactMap { RepoEntityWrapper(repo: $0) }
            
            self.presenter.set(results: wrappedArr, query: "", page: 0)
        } catch let error {
            self.presenter.showAlertView(with: error.localizedDescription)
        }
    }
}
