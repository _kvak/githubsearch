//
//  HomePresenter.swift
//  GithubRepoSearch
//
//  Created by Andre Kvashuk on 10/10/18.
//  Copyright © 2018 Andre Kvashuk. All rights reserved.
//

import Foundation
import WebKit

protocol HomePresenterProtocol: class {
    var router: HomeRouterProtocol! { set get }
    var canViewEditRows: Bool { get }

    func onSearchButtonClicked(query: String)
    func onStopButtonClicked()
    func onLogoutButtonClicked()
    func onLoginButtonClicked()
    func onRecentButtonClicked()
    
    func onMoveFrom(sourceIndex: Int, to destIndex: Int)
    func onCellSelectedWith(entity: HomeViewEntity)
    
    func set(results: [HomeViewEntity], query: String, page: Int)
    func onDeleteFromTable(entity: HomeViewEntity)
    func set(isLoggedIn: Bool)
    
    func onNeedNextPage()
    func authorizeWith(code: String)
    func configureView()

    func showHUD()
    func hideHUD()
    func showAlertView(with text: String)
}

@objc protocol HomeViewEntity: class {
    var repoName: String { get }
    var repoUrl: String { get }
    var repoId: Int64 { get }
    var repoStars: Int32 { get }
    var isViewed: Bool { get }
}

class RepoEntityWrapper {
    let repo: RepoEntity
    
    init(repo: RepoEntity) {
        self.repo = repo
    }
}

extension RepoEntityWrapper: RepoDetailEntity {
    
    var url: URL? {
        return URL(string: self.repo.urlString!)
    }

}

extension RepoEntityWrapper: HomeViewEntity {
    var repoName: String {
        return self.repo.name!
    }
    
    var repoUrl: String {
        return self.repo.repoUrl
    }
    
    var repoId: Int64 {
        return self.repo.repoId
    }
    
    var repoStars: Int32 {
        return self.repo.repoStars
    }
    
    var isViewed: Bool {
        return true
    }
    
}

class HomePresenter: NSObject, HomePresenterProtocol {
   
    enum HomePresenterConfig {
        static let pageSize = 30
    }
    
    var router: HomeRouterProtocol!
    weak var view: HomeViewProtocol!
    var interactor: HomeInteractorProtocol!
    
    fileprivate var repos = [HomeViewEntity]()
    fileprivate var currentQuery: String?
    fileprivate var pagesInProgress = [Int]()
    
    fileprivate let clientId = "Iv1.a5e15d174b9064c5"
    fileprivate let redirect = "https://githubsearchapp-62742.firebaseapp.com/__/auth/handler"
    
    var canViewEditRows: Bool {
        if let _ = self.repos as? [RepoEntityWrapper] {
            return true
        } else {
            return false
        }
    }
    
    required init(view: HomeViewProtocol) {
        self.view = view
    }
    
    func onStopButtonClicked() {
        self.currentQuery = nil
        self.view.hideHUD()
    }
    
    func onRecentButtonClicked() {
        self.currentQuery = ""
        self.interactor.getRecentRepos()
    }
    
    func onDeleteFromTable(entity: HomeViewEntity) {
        let wrapper = entity as! RepoEntityWrapper
        self.interactor.deleteFromDB(wrapper: wrapper)
    }
    
    func onMoveFrom(sourceIndex: Int, to destIndex: Int) {
        let unwrapped = self.repos as! [RepoEntityWrapper]
        let isBottomUp = sourceIndex > destIndex
        
        print("didMove")
        let start = min(sourceIndex, destIndex)
        let end = max(sourceIndex, destIndex)
        
        let slice: ArraySlice<RepoEntityWrapper>
        
        if !isBottomUp {
            let tempSlice = unwrapped[start...end].reversed()
            let array = Array(tempSlice)
            
            slice = array[array.startIndex...array.index(before: array.endIndex)]
        } else {
            slice = unwrapped[start...end]
        }
        
        var startIndex = slice.startIndex
        var nextIndex = slice.index(after: startIndex)
        let endIndex = slice.endIndex
        let tempSortIndex: Int32 = slice.first!.repo.sortIndex
        
        while nextIndex != endIndex {
            let current = slice[startIndex]
            let next = slice[nextIndex]
            print("was \(current.repo.sortIndex) will be \(next.repo.sortIndex)")
            current.repo.sortIndex = next.repo.sortIndex
            
            startIndex = nextIndex
            nextIndex = slice.index(after: nextIndex)
        }
        slice.last!.repo.sortIndex = tempSortIndex
        
        let movedEntity = self.repos[sourceIndex]
        self.repos.remove(at: sourceIndex)
        self.repos.insert(movedEntity, at: destIndex)
        try! AppDelegate.context.save()
        
        self.view.set(entities: self.repos)
    }
    
    func set(results: [HomeViewEntity], query: String, page: Int) {
        guard currentQuery == query else {
            return
        }

        if page == 0 {
            self.repos = results
        } else {
            self.repos.append(contentsOf: results)
        }
        self.view.hideHUD()
        self.view.set(entities: self.repos)
        
        let isInitialResultEmpty = page == 0 && results.isEmpty
        
        if isInitialResultEmpty {
            self.view.showAlertView(with: "No results for query \"\(query)\"")
        }
    }
    
    func onSearchButtonClicked(query: String) {
        self.view.hideKeyboard()
        self.view.showHUD()
        self.currentQuery = query
        self.pagesInProgress.removeAll()
        
        self.interactor.searchRepos(keyword: query, page: 0, pageSize: HomePresenterConfig.pageSize)
    }
    
    func onLogoutButtonClicked() {
        self.interactor.logout()
    }
    
    func onLoginButtonClicked() {
        let urlString = "https://github.com/login/oauth/authorize?client_id=\(clientId)"
        let url = URL(string: urlString)
        let urlRequest = URLRequest(url: url!)
        
        self.view.loadURLRequest(request: urlRequest, delegate: self)
    }
    
    func onCellSelectedWith(entity: HomeViewEntity) {
        self.router.showRepoDetailScene(entity: entity)
    }
    
    func onNeedNextPage() {
        guard let query = self.currentQuery else { return }
        let currentPage = self.repos.count / HomePresenterConfig.pageSize
        let nextPage = Int(currentPage) + 1
        
        guard !pagesInProgress.contains(nextPage) else { return }
        self.pagesInProgress.append(nextPage)
        
        self.interactor.searchRepos(keyword: query, page: nextPage, pageSize: HomePresenterConfig.pageSize)
    }
    
    func authorizeWith(code: String) {
        self.interactor.authorizeWith(code: code)
    }
    
    func configureView() {
        self.view.set(isLoggedIn: self.interactor.isLoggedIn())
    }
    
    func set(isLoggedIn: Bool) {
        self.view.set(isLoggedIn: isLoggedIn)
    }
    
    func showHUD() {
        view.showHUD()
    }
    
    func hideHUD() {
        view.hideHUD()
    }
    
    func showAlertView(with text: String) {
        view.showAlertView(with: text)
    }
}

extension HomePresenter: WKNavigationDelegate {
    
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        let urlString = navigationAction.request.url!.absoluteString
        
        if urlString.contains(redirect) {
            decisionHandler(.cancel)
            
            if let code = urlString.components(separatedBy: "?").last?.components(separatedBy: "=").last {
                authorizeWith(code: code)
            }
            webView.removeFromSuperview()
        } else {
            decisionHandler(.allow)
        }
    }
    
}
