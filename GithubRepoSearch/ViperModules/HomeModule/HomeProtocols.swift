//
//  HomeProtocols.swift
//  GithubRepoSearch
//
//  Created by Andre Kvashuk on 10/10/18.
//  Copyright © 2018 Andre Kvashuk. All rights reserved.
//

import Foundation
import UIKit

protocol HomeInteractorProtocol: class {
    func getRecentRepos()
    func searchRepos(keyword: String, page: Int, pageSize: Int)
    func deleteFromDB(wrapper: RepoEntityWrapper)
    func authorizeWith(code: String)
    func isLoggedIn() -> Bool
    func logout()
}

protocol HomeRouterProtocol: class {
    func showRepoDetailScene(entity: HomeViewEntity)
    func prepare(for segue: UIStoryboardSegue, sender: Any?)
}

protocol HomeConfiguratorProtocol: class {
    func configure(with viewController: HomeViewController)
}
