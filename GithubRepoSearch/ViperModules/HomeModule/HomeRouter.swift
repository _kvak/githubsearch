//
//  HomeRouter.swift
//  GithubRepoSearch
//
//  Created by Andre Kvashuk on 10/10/18.
//  Copyright © 2018 Andre Kvashuk. All rights reserved.
//

import UIKit

class HomeRouter: HomeRouterProtocol {
    weak var viewController: HomeViewController!
    
    private let detailSegueId = "toDetailSegue"
    
    required init(viewController: HomeViewController) {
        self.viewController = viewController
    }
    
    func showRepoDetailScene(entity: HomeViewEntity) {
        viewController.performSegue(withIdentifier: detailSegueId, sender: entity)
    }
    
    func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == detailSegueId {
            let destinationVC = segue.destination as! RepoDetailViewController
            let entity = sender as! RepoDetailEntity
            destinationVC.set(model: entity)
        }
    }
}
