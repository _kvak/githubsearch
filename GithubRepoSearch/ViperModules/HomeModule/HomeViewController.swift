//
//  ViewController.swift
//  GithubRepoSearch
//
//  Created by Andre Kvashuk on 10/10/18.
//  Copyright © 2018 Andre Kvashuk. All rights reserved.
//

import UIKit
import WebKit


protocol HomeViewProtocol: class {
    func showHUD()
    func hideHUD()
    func showAlertView(with text: String)
    func hideKeyboard()
    func set(entities: [HomeViewEntity])
    func set(isLoggedIn: Bool)
    func loadURLRequest(request: URLRequest, delegate: WKNavigationDelegate)
}

class HomeViewController: UIViewController {
    
    let configurator: HomeConfiguratorProtocol = HomeConfigurator()
    var presenter: HomePresenterProtocol!
    
    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var activityIndicatorContainer: UIView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    @IBOutlet weak var bottomViewHeightConstraint: NSLayoutConstraint!
    fileprivate var entities = [HomeViewEntity]()
    
    fileprivate var isTableEditing = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configurator.configure(with: self)
        presenter.configureView()
        
        setupTableView()
        setupNotifications()
    }
    
    func set(entities: [HomeViewEntity]) {
        self.entities = entities
        self.tableView.reloadData()
    }
    
    @IBAction func handleRecentButton(_ sender: Any) {
        self.presenter.onRecentButtonClicked()
    }
    
    @IBAction func handleStopButton(_ sender: Any) {
        self.presenter.onStopButtonClicked()
    }
    
    @IBAction func handleLoginButton(_ sender: Any) {
        self.presenter.onLoginButtonClicked()
    }
    
    @IBAction func handleSearchButton(_ sender: Any) {
        self.presenter.onSearchButtonClicked(query: self.textField.text!)
    }
    
    @IBAction func handleLogoutButton(_ sender: Any) {
        self.presenter.onLogoutButtonClicked()
    }
    
    @IBAction func handleReorderButton(_ sender: Any) {
        let newValue = !self.isTableEditing
        self.isTableEditing = newValue
        
        self.tableView.setEditing(newValue, animated: true)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        self.presenter.router.prepare(for: segue, sender: sender)
    }
}

private extension HomeViewController {
    
    func setupTableView() {
        tableView.register(RepoTableViewCell.defaultNib(), forCellReuseIdentifier: "cell")
        let frame = CGRect.zero
        let footer = UIView(frame: frame)
        tableView.tableFooterView = footer
    }
    
    func setupNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(handleKeyboardAppear(notification:)), name: UIWindow.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(handleKeyboardDissappear(notification:)), name: UIWindow.keyboardWillHideNotification, object: nil)
    }
    
    @objc func handleKeyboardAppear(notification: Notification) {
        let userInfo = notification.userInfo as? [String : Any] ?? [:]
        let frame = userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? CGRect
        let height = frame?.height ?? 0
        
        setKeyboard(height: height)
    }
    
    @objc func handleKeyboardDissappear(notification: Notification) {
        setKeyboard(height: 0)
    }
    
    func setKeyboard(height: CGFloat) {
        self.bottomViewHeightConstraint.constant = height
        
        DispatchQueue.main.async {
            UIView.animate(withDuration: 0.3) {
                self.view.layoutIfNeeded()
            }
        }
    }
}

extension HomeViewController: HomeViewProtocol {
    func set(isLoggedIn: Bool) {
        self.navigationItem.leftBarButtonItem?.isEnabled = !isLoggedIn
        self.navigationItem.rightBarButtonItem?.isEnabled = isLoggedIn
    }
    
    func showHUD() {
        self.activityIndicatorContainer.isHidden = false
    }
    
    func hideHUD() {
        self.activityIndicatorContainer.isHidden = true
    }
    
    func showAlertView(with text: String) {
        let alert = UIAlertController(title: "", message: text, preferredStyle: .alert)
        let cancelAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
   
        alert.addAction(cancelAction)
        
        present(alert, animated: true, completion: nil)
    }
    
    func hideKeyboard() {
        self.textField.resignFirstResponder()
    }
    
    func loadURLRequest(request: URLRequest, delegate: WKNavigationDelegate) {
        let webView = WKWebView(frame: self.view.frame)
        self.view.addSubview(webView)
        
        webView.load(request)
        webView.navigationDelegate = delegate
    }
}

extension HomeViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.entities.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! RepoTableViewCell
        
        let entity = self.entities[indexPath.row]
        cell.set(model: entity)
        
        return cell
    }
    
}

extension HomeViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == self.entities.count - 10 {
            self.presenter.onNeedNextPage()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let entity = self.entities[indexPath.row]
        self.presenter.onCellSelectedWith(entity: entity)
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return self.presenter.canViewEditRows
    }
    
    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCell.EditingStyle {
        return .delete
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let delete = UITableViewRowAction(style: .destructive, title: "Delete") { (action, indexPath) in
            let entity = self.entities[indexPath.row]
            
            self.tableView.beginUpdates()
            self.entities.remove(at: indexPath.row)
            self.tableView.deleteRows(at: [indexPath], with: .left)
            self.tableView.endUpdates()
            
            self.presenter.onDeleteFromTable(entity: entity)
        }

        return [delete]
    }
    
    func tableView(_ tableView: UITableView, moveRowAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        self.presenter.onMoveFrom(sourceIndex: sourceIndexPath.row, to: destinationIndexPath.row)
    }
}
