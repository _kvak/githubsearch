//
//  RepoTableViewCell.swift
//  GithubRepoSearch
//
//  Created by Andre Kvashuk on 10/14/18.
//  Copyright © 2018 Andre Kvashuk. All rights reserved.
//

import UIKit

class RepoTableViewCell: UITableViewCell {
    
    @IBOutlet weak var starsLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var isViewLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        separatorInset = .zero
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func set(model: HomeViewEntity) {
        starsLabel.text = "Stars: \(model.repoStars)"
        nameLabel.text = model.repoName
        isViewLabel.isHidden = !model.isViewed
    }
}
